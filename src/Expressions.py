class Expressions:
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def __str__(self):
        return 'Expressions (%d, %d)' % (self.a, self.b)

    # def __add__(self, other):
    #     return Expressions(self.a + other.a, self.b + other.b)

    def add_function(self):
        print('Result of Addition')
        print(self.a + self.b)
        return self.a + self.b

    def sub_function(self):
        print('Result of Subtraction')
        print(self.a - self.b)
        return self.a - self.b

    def mul_function(self):
        print('Result of Multiplication')
        print(self.a * self.b)
        return self.a * self.b

    def div_function(self):
        try:
            print('Result of Division')
            print(self.a / self.b)
        except Exception:
            print("Error: have problem with data")
        else:
            print("Successfully")
        return self.a / self.b

    def choose_method(self, method):
        if method == 1:
            return self.add_function()
        if method == 2:
            return self.sub_function()
        if method == 3:
            return self.mul_function()
        if method == 4:
            return self.div_function()

#
# firstStep = input('Please select from the following options: \n [1] Addition (+) \n [2] Subtraction (-) \n [3] '
#                   'Multiplication (*) \n [4] Division (/) \n')
# firstStep = int(firstStep)
#
# # Ask for the number and store it in userNumber
# userNumberOne = input('Give me first integer number: ')
#
# # Make sure the input is an integer number
# userNumberOne = int(userNumberOne)
#
# # Ask for the number and store it in userNumber
# userNumberSecond = input('Give me second integer number: ')
#
# # Make sure the input is an integer number
# userNumberSecond = int(userNumberSecond)
# exp = Expressions(userNumberOne, userNumberSecond)
# # print(exp.a)
# # print(exp.b)
# exp.choose_method(firstStep)
