import unittest

from src.Expressions import Expressions


class TestStringMethods(unittest.TestCase):
    exp = Expressions(6, 3)

    def test_upper(self):
        self.assertEqual('foo'.upper(), 'FOO')

    def test_func_add(self):
        self.assertEqual(self.exp.choose_method(1), 9)
    def test_func_sub(self):
        self.assertEqual(self.exp.choose_method(2), 3)
    def test_func_mul(self):
        self.assertEqual(self.exp.choose_method(3), 18)
    def test_func_div(self):
        self.assertEqual(self.exp.choose_method(4), 2)




if __name__ == '__main__':
    unittest.main()
